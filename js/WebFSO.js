
import JSZip from "jszip"
import saveAs from "file-saver"
import $ from 'jquery'
import WX from '../WX'
export default class WebFSO {
    static async save() {
        console.log("save")
        // 压缩文件
        let file_list = window.FSO;
        const filePaths = Object.keys(file_list);
        if (filePaths.length <= 0) {
            return;
        }
        var zip = new JSZip();

        for (let filePath of filePaths) {
            const file = file_list[filePath];
            if (file) {
                zip.file(
                    filePath.substring(WX.env.USER_DATA_PATH.length + 1),
                    file.data
                );
            }
        }
        // 异步生成压缩文件
        const content = await zip.generateAsync({ type: "blob" });
        // 保存到本地
        saveAs(content, "file.zip")
        localStorage.setItem("onekit_fso", true)
        alert("开始保存")

    }

    static load() {
        $.confirm({
            title: '是否载入本地文件?',
            content: '',
            type: 'green',
            buttons: {
                ok: {
                    text: 'ok!',
                    btnClass: 'btn-primary',
                    keys: ['enter'],
                    action: async () => {
                        const eChooseImage = document.createElement('input')
                        eChooseImage.setAttribute('type', 'file')
                        eChooseImage.setAttribute('style', 'visibility: hidden')

                        eChooseImage.setAttribute('id', 'eFSO')
                        eChooseImage.setAttribute('accept', 'zip/*')

                        eChooseImage.addEventListener('change', async (e) => {
                            const { files } = await JSZip.loadAsync(e.target.files[0]); // 1) read the Blob

                            for (let filePath of Object.keys(files)) {
                                const file = files[filePath];
                                if (file.dir) {
                                    window.FSO[WX.env.USER_DATA_PATH + '/' + filePath] = null;
                                } else {
                                    // file 转 base64 转 blob
                                    const blob = await file.async("blob");
                                    window.FSO[WX.env.USER_DATA_PATH + '/' + filePath] = {
                                        createTime: file.date.getTime(),
                                        data: blob,
                                        size: blob.size
                                    };
                                }
                            }
                        })
                        // console.log(window.FSO)

                        document.body.appendChild(eChooseImage)
                        eChooseImage.click()
                    },
                },
                cancel: function () { },
            },
        })
    }

}
