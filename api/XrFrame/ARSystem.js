export default class ARSystem {
    onAdd() {

    }
    onRelease() {

    }
    onRemove() {

    }
    onTick() {

    }
    onUpdate() {

    }
    set priority(priority) {

    }
    get priority() {

    }
    set schema(schema) {

    }
    get schema() {

    }

    get arModes() {

    }
    get arVersion() {

    }
    get el() {

    }
    get posCount() {

    }
    get ready() {

    }
    get scene() {

    }
    get supported() {

    }
    get version() {

    }
    forceSetViewMatrix() { }
    getARRawData() { }
    getData() { }
    placeHere() { }
    resetPlane() { }
    setData() { }
    setDataOne() { }


}