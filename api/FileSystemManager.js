import PROMISE from 'oneutil/PROMISE'
import BLOB from 'oneutil/BLOB'
import TASK from 'oneutil/TASK'
import TheKit from '../js/TheKit'
import State from './State'
import JsZip from 'jszip'
export default class FileSystemManager {
  constructor(FSO_OBJ) {
    this.fso = FSO_OBJ
  }

  accessSync(wx_path) {
    if (!wx_path) throw new Error('path is invalid')
    if (wx_path.substr(0, 6) !== 'wxfile') throw new Error('Browser is not support read the usr disk')
    if (Object.keys(window.TEMP).indexOf(wx_path) !== -1) {
      return true
    } else {
      throw new Error(`accessSync:fail no such file or directory, accessSync ${wx_path}`)
    }
  }

  access(wx_object) {
    const wx_path = wx_object.path
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null

    PROMISE(SUCCESS => {
      if (this.accessSync(wx_path)) {
        const wx_res = {
          errMsg: 'access: ok'
        }
        SUCCESS(wx_res)
      } else {
        const wx_res = {
          errMsg: `access:fail no such file or directory, access ${wx_path}`
        }
        throw Error(wx_res)
      }
    }, wx_success, wx_fail, wx_complete)
  }

  saveFileSync(tempFilePath, filePath) {

    const blob = window.TEMP[tempFilePath]
    const filename = blob.type
    const savedFilePath = filePath || TheKit.createUserPath(filename)
    // const ext = tempFilePath.substring(tempFilePath.lastIndexOf(".") + 1)
    window.FSO[savedFilePath] = {
      createTime: new Date().getTime(),
      size: blob.size,
      data: blob
      // ext
    }
    console.log("window.FSO[savedFilePath]", window.FSO[savedFilePath])
    return savedFilePath

  }

  saveFile(wx_object) {
    const wx_tempFilePath = wx_object.tempFilePath
    const wx_filePath = wx_object.filePath || TheKit.createUserPath(wx_tempFilePath)
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null
    ////////////////////////
    PROMISE((SUCCESS) => {
      const savedFilePath = this.saveFileSync(wx_tempFilePath, wx_filePath)
      const wx_res = {
        errMsg: 'saveFile: ok',
        savedFilePath,
      }
      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)
  }

  getSavedFileList(wx_object) {
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    console.log(window.FSO)
    PROMISE(SUCCESS => {
      const wx_fileList = []
      for (var filePath of Object.keys(window.FSO)) {
        const file = window.FSO[filePath]
        wx_fileList.push({
          filePath: filePath,
          size: file.size,
          createTime: file.createTime
        })
      }
      // const fileList = this.fso.FSO_LIST_
      const wx_res = {
        errMsg: 'getSavedFile: ok',
        fileList: wx_fileList
      }

      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)
  }

  removeSavedFile(wx_object) {
    const filePath = wx_object.filePath
    const wx_success = wx_object.success
    const wx_complete = wx_object.complete
    const wx_fail = wx_object.fail
    wx_object = null

    PROMISE(SUCCESS => {
      if (!filePath) return
      delete window.FSO[filePath]
      const wx_res = {
        errMsg: 'removeSavedFile: ok'
      }

      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)
  }

  copyFileSync(srcPath, destPath) {
    if (!srcPath) throw new Error("copyFile:fail parameter error: parameter.srcPath should be String instead of Undefined;")
    if (!destPath) throw new Error("copyFile:fail parameter error: parameter.srcPath should be String instead of Undefined;parameter.destPath should be String instead of Undefined;")
    if (destPath.startsWith('wxfile://usr')) {
      const blob = window.TEMP[srcPath]
      window.FSO[destPath] = {
        size: blob.size,
        createTime: new Date().getTime(),
        data: blob
      }
    } else {
      throw new Error('fail no such file or directory')
    }
  }

  copyFile(wx_object) {
    const srcPath = wx_object.srcPath
    const destPath = wx_object.destPath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete

    PROMISE(SUCCESS => {
      this.copyFileSync(srcPath, destPath)
      const wx_res = {
        errMsg: 'copyFile: ok'
      }
      SUCCESS(wx_res)

    }, wx_success, wx_fail, wx_complete)
  }

  getFileInfo(wx_object) {
    const wx_filePath = wx_object.filePath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete

    PROMISE(SUCCESS => {
      if (!wx_filePath) throw Error('getFileInfo:fail parameter error: parameter.filePath should be String instead of Undefined;')
      if (wx_filePath.startsWith('wxfile://usr') || wx_filePath.startsWith('wxfile://temp')) {
        if (!window.FSO[wx_filePath]) throw Error(`getFileInfo:fail permission denied, mkdirSync ${wx_filePath} at Object.eval [as mkdirSync]`)
        const blob = window.FSO[wx_filePath]
        console.log("//////////")
        // TheKit.tempFilepath2digest(wx_filePath)
        const digest = ""
        const wx_res = {
          digest,
          errMsg: 'getFileInfo: ok',
          size: blob.size
        }

        SUCCESS(wx_res)
      } else {
        throw new Error('fail no such file or directory')
      }

    }, wx_success, wx_fail, wx_complete)
  }

  mkdirSync(wx_dirPath) {
    if (!wx_dirPath.startsWith('wxfile://usr')) throw Error(`mkdirSync:fail permission denied, mkdirSync ${wx_dirPath} at Object.eval [as mkdirSync]`)
    if (!wx_dirPath.endsWith("/")) {
      wx_dirPath += "/"
    }
    if (window.FSO[wx_dirPath]) throw new Error(`mkdirSync:fail file already exists, mkdirSync ${wx_dirPath} at Object.eval [as mkdirSync]`)
    window.FSO[wx_dirPath] = null

  }

  mkdir(wx_object) {
    const wx_dirPath = wx_object.wx_dirPath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null
    ///////////////////////
    PROMISE(SUCCESS => {
      this.mkdirSync(wx_dirPath)
      const wx_res = {
        errMsg: 'mkdir: ok'
      }
      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)
  }

  readdirSync(wx_dirPath) {
    try {
      if (!wx_dirPath.startsWith('wxfile://usr')) throw Error
      let list_dir = window.FSO
      let DIR_ARRAY = []
      for (let filePath of Object.keys(list_dir)) {
        if (!filePath.startsWith(wx_dirPath)) {
          continue
        }
        DIR_ARRAY.push(filePath.substring(wx_dirPath.length))

      }
      return DIR_ARRAY
    } catch (e) {
      throw Error(`readdirSync:fail permission denied, readdirSync ${wx_dirPath} at Object.eval [as mkdirSync]`)
    }
  }

  readdir(wx_object) {
    const wx_dirPath = wx_object.wx_dirPath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    PROMISE(SUCCESS => {
      let DIR_ARRAY = this.readdirSync(wx_dirPath)
      const wx_res = {
        errMsg: 'readdir: ok',
        files: DIR_ARRAY
      }
      SUCCESS(wx_res)

    }, wx_success, wx_fail, wx_complete)
  }

  async readFileSync(wx_filePath, wx_encoding, wx_position, wx_length) {
    if (wx_filePath.startsWith('wxfile://usr') && wx_filePath.startsWith('wxfile://temp')) throw Error(`readdirSync:fail permission denied, readdirSync ${wx_filePath} at Object.eval [as mkdirSync]`)

    const file = window.FSO[wx_filePath]
    console.log("readFileSync", file)
    if (!file) return null
    if (wx_encoding) {
      function blob2string(blob) {
        return new Promise(resolve => {
          BLOB.blob2string(blob, str => {
            resolve(str)
          })
        })
      }
      const str = await blob2string(file.data)
      console.log("string====================", str)
      return str.substr(wx_position, wx_length)
    } else {
      function blob2arrbuffer(blob) {
        return new Promise(resolve => {
          BLOB.blob2arrbuffer(blob, ab => {
            resolve(ab)
          })
        })
      }
      return await blob2arrbuffer(file.data)
    }
    // try{
    //   let blob
    //   if(filePath.substr(0, 12) === 'wxfile://usr') blob = window.FSO[filePath]
    //   if(filePath.substr(0, 12) === 'wxfile://temp') blob = window.TEMP[filePath]
    //   else throw Error (`readdirSync:fail permission denied, readdirSync ${wx_dirPath} at Object.eval [as mkdirSync]`)
    //   switch(encoding) {
    //     case 'ascii':

    //     break;
    //     default:
    //       blob = blob
    //   }
    // console.warn(`[warn]readFileSync: it's not support, you can use the [readFile] instead.`)
    //   return wx_res
    // }catch (e) {
    //   throw (e)
    // }
  }

  readFile(wx_object) {
    const filePath = wx_object.filePath
    const encoding = wx_object.encoding
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete

    PROMISE(SUCCESS => {
      let blob
      if (filePath.startsWith('wxfile://usr')) blob = window.FSO[filePath]
      if (filePath.startsWith('wxfile://temp')) blob = window.TEMP[filePath]
      // else throw Error (`readdirSync:fail permission denied, readdirSync ${filePath} at Object.eval [as mkdirSync]`)
      let result = {
        errMsg: 'readFile: ok',
        data: ''
      }
      switch (encoding) {
        case 'ascii':
          TheKit.blob2ascii(blob, wx_res => {
            result.data = wx_res
            SUCCESS(result)
          })
          break
        case 'base64':
          TheKit.blobToBase64(blob, wx_res => {
            result.data = wx_res
            SUCCESS(result)
          })
          break
        case 'binary':
          TheKit.blob2binary(blob, wx_res => {
            result.data = wx_res
            SUCCESS(result)
          })
          break
        case 'hex':
          TheKit.blob2hex(blob, wx_res => {
            result.data = wx_res
            SUCCESS(result)
          })
          break
        case 'latin1':
          TheKit.blob2hex(blob, wx_res => {
            result.data = wx_res
            SUCCESS(result)
          })
          break
        case 'utf-8' || 'utf8':
          TheKit.blob2string(blob, wx_res => {
            result.data = wx_res
            SUCCESS(result)
          })
          break
        default:
          TheKit.blob2arrbuffer(blob, wx_res => {
            result.data = wx_res
            SUCCESS(result)
          })
      }

    }, wx_success, wx_fail, wx_complete)
  }

  renameSync(wx_oldPath, wx_newPath) {
    if (!wx_oldPath.startsWith('wxfile://usr')) throw Error(`renameSync:fail permission denied, readdirSync ${wx_oldPath} at Object.eval [as mkdirSync]`)
    if (!wx_newPath.startsWith('wxfile://usr')) throw Error(`renameSync:fail permission denied, readdirSync ${wx_newPath} at Object.eval [as mkdirSync]`)
    if (window.FSO[wx_newPath]) throw Error(`renameSync:fail filename already exists, mkdirSync ${wx_newPath} at Object.eval [as mkdirSync]`)
    window.FSO[wx_newPath] = window.FSO[wx_oldPath]
    delete window.FSO[wx_oldPath]

  }

  rename(wx_object) {
    const wx_oldPath = wx_object.oldPath
    const wx_newPath = wx_object.newPath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete

    PROMISE(SUCCESS => {
      this.renameSync(wx_oldPath, wx_newPath)
      const wx_res = {
        errMsg: 'rename: ok'
      }
      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)
  }

  rmdirSync(wx_dirPath) {
    if (!wx_dirPath) throw new Error('invoke error: Error: path is invalid }')
    if (!wx_dirPath.startsWith('wxfile://usr')) throw Error(`mkdirSync:fail permission denied, mkdirSync ${wx_dirPath}`)
    let list_dir = window.FSO
    for (let filePath of Object.keys(list_dir)) {
      if (filePath.startsWith(wx_dirPath)) {
        delete window.FSO[filePath]
      }

    }

  }

  rmdir(wx_object) {
    const wx_filePath = wx_object.filePath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null

    PROMISE(SUCCESS => {
      this.rmdirSync(wx_filePath)
      const wx_res = {
        errMsg: 'rmdir: ok'
      }
      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)
  }

  statSync(wx_path, recursive = false) {
    let size = 0
    let blob = {}
    if (!wx_path) throw new Error('invoke error: Error: path is invalid }')
    if (wx_path.startsWith('wxfile://usr')) blob = window.FSO
    if (wx_path.startsWith('wxfile://temp')) blob = window.TEMP
    if (wx_path.lastIndexOf(".") === -1) {
      let list_stat = []
      const e = {
        mode: 33206,
        size: 0,
        lastAccessedTime: window.FSO[`${wx_path}_current_time`] || window.TEMP[`${wx_path}_current_time`] || new Date().getTime(),
        lastModifiedTime: window.FSO[`${wx_path}_current_time`] || window.TEMP[`${wx_path}_current_time`] || new Date().getTime(),
        wx_path
      }
      list_stat.push({
        path: "/",
        stats: e
      })
      if (recursive) {
        for (let filePath of Object.keys(blob)) {
          if (filePath.startsWith(wx_path)) {
            const e = {
              mode: 33206,
              size: blob[filePath].size,
              lastAccessedTime: blob[`${filePath}_current_time`] || new Date().getTime(),
              lastModifiedTime: blob[`${filePath}_current_time`] || new Date().getTime(),
              filePath
            }
            list_stat.push({
              path: filePath.substring(wx_path.length),
              stats: e
            })
          }
        }
      }
      return list_stat
    } else {
      if (!window.FSO[wx_path] && !window.TEMP[wx_path]) throw new Error(`mkdirSync:fail permission denied, mkdirSync ${wx_path}`)

      const e = {
        mode: 33206,
        size: blob[wx_path].size,
        lastAccessedTime: window.FSO[`${wx_path}_current_time`] || window.TEMP[`${wx_path}_current_time`] || new Date().getTime(),
        lastModifiedTime: window.FSO[`${wx_path}_current_time`] || window.TEMP[`${wx_path}_current_time`] || new Date().getTime(),
        wx_path
      }
      return new State(e)
    }
  }

  stat(wx_object) {
    const wx_path = wx_object.path
    const wx_recursive = wx_object.recursive || false
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null

    PROMISE(SUCCESS => {
      const stats = this.statSync(wx_path, wx_recursive)
      const wx_res = {
        errMsg: "stat:ok",
        stats
      }
      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)

  }

  unlinkSync(wx_filePath) {
    if (!wx_filePath) throw new Error(`invoke error: Error: path is invalid }`)
    if (!window.FSO[wx_filePath]) throw new Error(`unlink:fail no such file or directory "${wx_filePath}"`)
    delete window.FSO[wx_filePath]
  }

  unlink(wx_object) {
    const wx_filePath = wx_object.filePath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null

    PROMISE(SUCCESS => {
      this.unlinkSync(wx_filePath)
      const wx_res = {
        errMsg: 'unlink: ok'
      }
      SUCCESS(wx_res)
    }, wx_success, wx_fail, wx_complete)
  }

  unzip(wx_object) {
    const wx_zipFilePath = wx_object.zipFilePath
    const wx_targetPath = wx_object.targetPath
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete
    wx_object = null

    PROMISE(SUCCESS => {
      if (!wx_targetPath.startsWith('wxfile://usr')) throw Error(`uzip: wx_fail permission denied, mkdirSync ${wx_targetPath}`)
      if (!window.FSO[wx_zipFilePath]) throw Error(`Your zip file is not defined`)
      const blob = window.FSO[wx_zipFilePath].data
      const JSZIP = new JsZip()
      console.log("blob", blob)
      this.mkdirSync(wx_targetPath)
      const web_targetPath = wx_targetPath.endsWith("/") ? wx_targetPath : wx_targetPath + "/"
      JSZIP.loadAsync(blob).then(ziplist => {
        console.log("ziplist", ziplist.files)
        const filenames = Object.keys(ziplist.files)
        TASK(filenames, (filename, callback) => {
          JSZIP.file(filename).async('blob').then(blob => {
            window.FSO[`${web_targetPath}${filename}`] = {
              createTime: new Date().getTime(),
              size: blob.size,
              data: blob
            }
            callback()
          })

        }, () => {
          const wx_res = {
            errMsg: 'unzip: ok',
          }
          SUCCESS(wx_res)
        })
      })
    }, wx_success, wx_fail, wx_complete)
  }

  writeFileSync(filePath, data, encoding) {
    // var data = '<b style="font-size:32px;color:red;">次碳酸钴</b>';
    // var blob = new Blob([data], { "type": "text/html" });
    // console.log("blob", blob)
    if (!filePath) throw new Error('invoke error: Error: path is invalid }')
    if (!filePath.startsWith('wxfile://usr')) throw Error(`renameSync:fail permission denied, readdirSync ${filePath} at Object.eval [as mkdirSync]`)
    if (!data) throw Error('invoke error: Error: data is invalid }')
    var blob = {}
    if (encoding) {
      // 文本文件
      blob = new Blob([data], { "type": "txt" })
    } else {
      // 其他文件
      blob = new Blob([data])
    }
    window.FSO[filePath] = {
      createTime: new Date().getTime(),
      size: blob.size,
      data: blob
    }
    // console.warn(`Please use [writeFile] instead`)
  }

  writeFile(wx_object) {
    const filePath = wx_object.filePath
    const data = wx_object.data
    const encoding = wx_object.encoding
    const wx_success = wx_object.success
    const wx_fail = wx_object.fail
    const wx_complete = wx_object.complete

    // if (encoding === 'ascii' || encoding === 'utf-8' || encoding === 'utf8') {
    //   encoding = 'string'
    // } else if (encoding === 'binary' || encoding === 'hex') {
    //   encoding === 'blob'
    // } else if (!encoding) {
    //   encoding = 'arraybuffer'
    // }
    wx_object = null
    PROMISE(SUCCESS => {
      this.writeFileSync(filePath, data, encoding)
      // const JSZIP = new JsZip()
      // const filename = filePath.substr(filePath.lastIndexOf('/') + 1)
      // JSZIP.file(filename, data)
      // JSZIP.file(filename).async(encoding).then(data => {
      // window.FSO[filePath] = data
      const wx_res = {
        errMsg: 'writeFile: ok'
      }
      SUCCESS(wx_res)
      // })
    }, wx_success, wx_fail, wx_complete)
  }
}
