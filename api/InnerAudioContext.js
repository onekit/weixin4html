export default class InnerAudioContext {
  constructor(web_audioContext) {
    this.web_audioContext = web_audioContext
    this.web_bufferSource = this.web_audioContext.createBufferSource();
  }

  set src(src) {
    this._src = src
  }
  get src() {
    return this._src
  }
  set startTime(position) {
    this.web_bufferSource.currentTime=position
  }

  set autoplay(boolean) {
    this.web_bufferSource.autoplay=boolean
  }

  set loop(boolean) {
    this.web_bufferSource.loop=boolean
  }

  set obeyMuteSwitch(boolean) {
    this.web_bufferSource.muted=boolean
  }

  play() {
    const blob = window.TEMP[this.src] 
    BLOB.blob2arrbuffer(blob, arraybuffer => {
      this.web_audioContext.decodeAudioData(
        arraybuffer,
        (buffer) =>{
          this.web_bufferSource.buffer = buffer;
          this.web_bufferSource.connect(this.web_audioContext.destination);
          this.web_bufferSource.start(0); //立即播放
        },
        console.error
      );
    });
  }

  pause() {
    this.web_bufferSource.pause()
    this.onPause=()=>false
  }

  stop() {
    this.web_bufferSource.pause()
    this.web_bufferSource.currentTime=0
  }

  seek(position) {
    this.web_bufferSource.currentTime=position
  }

  destroy() {
    this.web_bufferSource.pause()
    this.web_bufferSource=null
  }

  onCanplay(callback) {
    this.web_bufferSource.addEventListener('canplay',()=>{
      callback()
    })
  }

  onPlay(callback) {
    this._playListenner = function _eventListener() {
      const result = arguments[0].path[0]
      const res = {
        autoplay: result.autoplay,
        buffered: result.buffered.length,
        currentTime: result.currentTime,
        duration: result.duration,
        errMsg: 'getAudioState: ok',
        isInPkg: false,
        loop: result.loop,
        obeyMuteSwitch: result.muted,
        paused: arguments[0].type === 'paused',
        realativeSrc: result.currentSrc,
        src: result.currentSrc,
        volume: arguments[0].path.length
      }
      callback(res)
    }
    this.web_bufferSource.addEventListener('play',this._playListenner)
  }

  onPause(callback) {
    this._pauseListenner = function _eventListener() {
      const result = arguments[0].path[0]
      const res = {
        autoplay: result.autoplay,
        buffered: result.buffered.length,
        currentTime: result.currentTime,
        duration: result.duration,
        errMsg: 'getAudioState: ok',
        isInPkg: false,
        loop: result.loop,
        obeyMuteSwitch: result.muted,
        paused: arguments[0].type === 'paused',
        realativeSrc: result.currentSrc,
        src: result.currentSrc,
        volume: arguments[0].path.length
      }
      callback(res)
    }
    this.web_bufferSource.addEventListener('pause',this._pauseListenner)
  }

  onStop(callback) {
    this._stopListenner = function _eventListener() {
      const result = arguments[0].path[0]
      const res = {
        autoplay: result.autoplay,
        buffered: result.buffered.length,
        currentTime: result.currentTime,
        duration: result.duration,
        errMsg: 'getAudioState: ok',
        isInPkg: false,
        loop: result.loop,
        obeyMuteSwitch: result.muted,
        paused: arguments[0].type === 'paused',
        realativeSrc: result.currentSrc,
        src: result.currentSrc,
        volume: arguments[0].path.length
      }
      callback(res)
    }
    this.web_bufferSource.addEventListener('stop',this._stopListenner)
  }

  onEnded(callback) {
    this._endedListenner = function _eventListener() {
      const result = arguments[0].path[0]
      const res = {
        autoplay: result.autoplay,
        buffered: result.buffered.length,
        currentTime: result.currentTime,
        duration: result.duration,
        errMsg: 'getAudioState: ok',
        isInPkg: false,
        loop: result.loop,
        obeyMuteSwitch: result.muted,
        paused: arguments[0].type === 'paused',
        realativeSrc: result.currentSrc,
        src: result.currentSrc,
        volume: arguments[0].path.length
      }
      callback(res)
    }
    this.web_bufferSource.addEventListener('ended',this._endedListenner)
  }

  onTimeUpdate(callback) {
    this._timeupdateListenner = function _eventListener() {
      callback()
    }
    this.web_bufferSource.addEventListener('timeupdate',this._timeupdateListenner)
  }

  onError(callback) {
    this._errorListenner = function _eventListener() {
      callback()
    }
    this.web_bufferSource.addEventListener('error',this._errorListenner)
  }

  onWaiting(callback) {
    this._waitingListenner = function _eventListener() {
      callback()
    }
    this.web_bufferSource.addEventListener('waiting',this._waitingListenner)
  }

  onSeeking(callback) {
    this._seekingListenner = function _eventListener() {
      callback()
    }
    this.web_bufferSource.addEventListener('seeking',this._seekingListenner)
  }

  onSeeked(callback) {
    this._seekedListenner = function _eventListener() {
      callback()
    }
    this.web_bufferSource.addEventListener('seeked',this._seekingListenner)
  }

  offPlay() {
    this.web_bufferSource.addEventListener('play',this._playListenner)
  }

  offPause() {
    this.web_bufferSource.addEventListener('pause',this._pauseListenner)
  }

  offStop() {
    this.web_bufferSource.addEventListener('stop',this._stopListenner)
  }

  offEnded() {
    this.web_bufferSource.addEventListener('ended',this._endedListenner)
  }

  offTimeUpdate() {
    this.web_bufferSource.addEventListener('timeupdate',this._timeupdateListenner)
  }

  offError() {
    this.web_bufferSource.addEventListener('error',this._errorListenner)
  }

  offWaiting() {
    this.web_bufferSource.addEventListener('waiting',this._waitingListenner)
  }

  offSeeking() {
    this.web_bufferSource.addEventListener('seeking',this._seekingListenner)
  }

  offSeeked() {
    this.web_bufferSource.addEventListener('seeked',this._seekingListenner)
  }
}