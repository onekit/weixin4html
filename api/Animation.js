import COLOR from 'oneutil/COLOR'

export default class WxAnimation {
    constructor(object) {
        this.option = this._fixOption(object)
        this._animations = [];
        this._buffer = [];
    }
    _fixOption(object = {}) {
        if (object.duration == null) {
            object.duration = 400
        }
        if (object.timingFunction == null) {
            object.timingFunction = 'linear'
        }
        if (object.delay == null) {
            object.delay = 0
        }
        if (object.transformOrigin == null) {
            object.transformOrigin = '50% 50% 0'
        }
        ///////////////////////////
        const option = {
            transformOrigin: object.transformOrigin
        }
        delete object.transformOrigin
        option.transition = object;
        return option;
    }
    scale(sx, sy) {
        if (sy == null) {
            sy = sx;
        }
        this._buffer.push({
            type: "scale",
            values: [parseFloat(sx), parseFloat(sy)]
        });
        return this;
    }

    scaleX(s) {
        this._buffer.push({
            type: "scaleX",
            values: [s]
        });
        return this;
    }

    scaleY(s) {
        this._buffer.push({
            type: "scaleY",
            values: [s]
        });
        return this;
    }

    //横向拉伸
    width(width) {
        if (typeof width === 'number') {
            width = `${width}px`
        }
        this._buffer.push({
            type: "style",
            values: ["width", width]
        });
        return this;
    }

    //纵向拉伸
    height(height) {
        if (typeof height === 'number') {
            height = `${height}px`
        }
        this._buffer.push({
            type: "style",
            values: ["height", height]
        });
        return this;
    }

    //旋转度数：rotation、rotationX、rotationY
    rotate(degree) {
        this._buffer.push({
            type: "rotate",
            values: [parseFloat(degree)]
        });
        return this;
    }

    rotateX(degree) {
        this._buffer.push({
            type: "rotateX",
            values: [parseFloat(degree)]
        });
        return this;
    }

    rotateY(degree) {
        this._buffer.push({
            type: "rotateY",
            values: [parseFloat(degree)]
        });
        return this;
    }

    rotateZ(degree) {
        this._buffer.push({
            type: "rotateZ",
            values: [parseFloat(degree)]
        });
        return this;
    }

    //透明动画
    opacity(alpha) {
        this._buffer.push({
            type: "style",
            values: ["opacity", alpha]
        });
        return this;
    }

    //平移：translationX、translationY
    translate(tx, ty) {
        if (ty == null) {
            ty = tx;
        }
        this._buffer.push({
            type: "translate",
            values: [parseFloat(tx), parseFloat(ty)]
        });
        return this;
    }

    translateX(tx) {
        this._buffer.push({
            type: "translateX",
            values: [parseFloat(tx)]
        });
        return this;
    }

    translateY(ty) {
        this._buffer.push({
            type: "translateY",
            values: [parseFloat(ty)]
        });
        return this;
    }

    //距离
    top(top) {
        if (typeof top === 'number') {
            top = `${top}px`
        }
        this._buffer.push({
            type: "style",
            values: ["top", top]
        });
        return this;
    }

    left(left) {
        if (typeof left === 'number') {
            left = `${left}px`
        }
        this._buffer.push({
            type: "style",
            values: ["left", left]
        });
        return this;
    }

    bottom(bottom) {
        if (typeof bottom === 'number') {
            bottom = `${bottom}px`
        }
        this._buffer.push({
            type: "style",
            values: ["bottom", bottom]
        });
        return this;
    }

    right(right) {
        if (typeof right === 'number') {
            right = `${right}px`
        }
        this._buffer.push({
            type: "style",
            values: ["right", right]
        });
        return this;
    }

    //背景颜色
    backgroundColor(backgroundColor) {
        this._buffer.push({
            type: "style",
            values: ["background-color", backgroundColor]
        });
        return this;
    }

    //倾斜
    skew(sx, sy) {
        if (sy == null) {
            sy = 0;
        }
        this._buffer.push({
            type: "skew",
            values: [parseFloat(sx), parseFloat(sy)]
        });
        return this;
    }

    //矩阵
    matrix(a, b, c, d, tx, ty) {
        this._buffer.push({
            type: "matrix",
            values: [a, b, c, d, tx, ty]
        });
        return this;
    }

    matrix3d(a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3, a4, b4, c4, d4) {
        this._buffer.push({
            type: "matrix3d",
            values: [a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3, a4, b4, c4, d4]
        });
        return this;
    }

    step(option) {
        let animation = { animationInfos: [], option: option ?this._fixOption(option) : this.option };
        for (let a = 0; a < this._buffer.length; a++) {
            let animationInfo = this._buffer[a];
            animation.animationInfos.push(animationInfo);
        }
        this._animations.push(animation);
        return this;
    }

    export() {
        let actions = [];
        for (let s = 0; s < this._animations.length; s++) {
            let animation = this._animations[s];
            let action = {};
            //定义集合
            let animates = [];
            for (let a = 0; a < animation.animationInfos.length; a++) {
                let animationInfo = animation.animationInfos[a];
                let animate = {};
                animate["type"] = animationInfo.type;
                animate["args"] = animationInfo.values;
                animates.push(animate);
            }
            action["animates"] = animates;
            //
            action["option"] = animation.option;
            //
            actions.push(action);
        }
        //
        this._buffer = [];
        this._animations = [];
        //
        return {
            actions: actions
        };
    }

    rotate3d(x, y, z, angle) {
        this._buffer.push({
            type: "rotate3d",
            values: [x, y, z, angle]
        });
        return this;
    }

    scale3d(sx, sy, sz) {
        this._buffer.push({
            type: "scale3d",
            values: [sx, sy, sz]
        });
        return this;
    }

    scaleZ(s) {
        this._buffer.push({
            type: "scaleZ",
            values: [s]
        });
        return this;
    }

    skewX(angle) {
        this._buffer.push({
            type: "skewX",
            values: [angle]
        });
        return this;
    }

    skewY(angle) {
        this._buffer.push({
            type: "skewY",
            values: [angle]
        });
        return this;
    }

    translate3d(tx, ty, tz) {
        this._buffer.push({
            type: "translate3d",
            values: [tx, ty, tz]
        });
        return this;
    }

    translateZ(tz) {
        this._buffer.push({
            type: "translateZ",
            values: [tz]
        });
        return this;
    }
}
