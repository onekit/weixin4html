import Vue from 'vue'
import App from './App.vue'
import WX from '../../WX'

const wx = new WX();
Vue.config.productionTip = false
document.addEventListener("visibilitychange", function () {

  if (document.hidden) {
    if (window.onAudioInterruptionBegin) {
      window.onAudioInterruptionBegin()
    }
    if (window.onAppHide) {
      const wx_path = OneKit.current().$route.path
      const wx_query = {
        params: "",
        query: ""
      }
      const wx_scene = undefined;
      const wx_referrerInfo = {};
      let wx_res = {
        path: wx_path,
        scene: wx_scene,
        query: wx_query,
        referrerInfo: wx_referrerInfo,
        shareTicket: undefined
      };
      window.onAppHide(wx_res)
    }

  } else {
    if (window.onAppShow) {
      const wx_path = OneKit.current().$route.path
      const wx_query = {
        params: "",
        query: ""
      }
      const wx_scene = undefined;
      const wx_referrerInfo = {};
      let wx_res = {
        path: wx_path,
        scene: wx_scene,
        query: wx_query,
        referrerInfo: wx_referrerInfo,
        shareTicket: undefined
      };
      window.onAppShow(wx_res)

    }
    if (window.onAudioInterruptionEnd) {
      window.onAudioInterruptionEnd()
    }
  }
})
new Vue({
  render: h => h(App),
}).$mount('#app')


const key = "key1";
const value = "456";
const result = wx.setStorageSync(key, value);
console.log(result);
const result2 = wx.getStorageSync(key);
console.log(result2);
